from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import UserRegisterForm, PerfilRegisterForm, UserUpdateForm, PerfilUpdateForm

def register(request):
    if request.method == 'POST':
        u_form = UserRegisterForm(request.POST)
        p_form = PerfilRegisterForm(request.POST, request.FILES)

        if p_form.is_valid() and p_form.is_valid():
            user = u_form.save()
            perfil = p_form.save(False)
            perfil.usuario = user
            perfil.save() 
            messages.success(request, f'Su cuenta ha sido Creada!!')
            return redirect('login')
    else: 
        u_form = UserRegisterForm()
        p_form = PerfilRegisterForm()
    
    context = {
        'u_form': u_form,
        'p_form': p_form,
    }
    return render(request, 'usuarios/register.html', context)


@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = PerfilUpdateForm(request.POST, request.FILES, instance=request.user.perfil)

        if u_form.is_valid() and p_form.is_valid():
            user = u_form.save()
            perfil = p_form.save(False)
            perfil.usuario = user
            perfil.save() 
            messages.success(request, f'Su cuenta ha sido Actualizada!!')
            return redirect('profile')
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = PerfilUpdateForm(instance=request.user.perfil)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }
    return render(request, 'usuarios/profile.html', context)
