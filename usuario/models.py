from django.db.models.deletion import CASCADE
from sena.models import Cuestionario
from django.db import models
from django.contrib.auth.models import User
from PIL import Image

class Region(models.Model):
    nombre = models.CharField(max_length=60, blank=False, null=False)

    def __str__(self):
        return self.nombre


class Perfil(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    imagen = models.ImageField(default='default.png', upload_to='profile_pics')
    region = models.ForeignKey(Region, on_delete=models.CASCADE, null=True)
    nivel = models.IntegerField(default=0, null=True)

    def __str__(self):
        return self.usuario.username
    
    def save(self):
        super().save()
        img = Image.open(self.imagen.path)
        if img.height > 300 or img.width > 300:
            output_size = (300,300)
            img.thumbnail(output_size)
            img.save(self.imagen.path)
