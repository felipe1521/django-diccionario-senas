# Generated by Django 3.0.8 on 2021-05-08 18:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usuario', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Ciudad',
            new_name='Region',
        ),
    ]
