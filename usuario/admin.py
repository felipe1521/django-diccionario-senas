from django.contrib import admin
from .models import Region, Perfil

admin.site.register(Region)
admin.site.register(Perfil)
