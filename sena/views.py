from django.shortcuts import render
from django.core.paginator import Paginator
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView
from .forms import SenaForm
from .filters import SenaFilter
from .models import Sena, Cuestionario, Categoria


def about(request):
    return render(request,'sena/about.html')


def categories(request):
    categorias = Categoria.objects.all()
    context = { 'categorias': categorias }
    return render(request,'sena/categories.html', context)


def questionary(request, id):
    cuestionario = Cuestionario.objects.filter(categoria=id)
    paginator = Paginator(cuestionario, 1)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    progreso = 10
    if page_number:
        progreso = int(page_number)*10
    print(progreso)
    return render(request, 'sena/questionary.html',{'page_obj' : page_obj, 'progreso' : progreso})


def home(request):
    senas = Sena.objects.all()

    filtro = SenaFilter(request.GET, queryset=senas)
    senas = filtro.qs
    context = {'senas': senas,
                'filtro': filtro }
    return render(request, 'sena/home.html', context)


class DetailSena(DetailView):
    model = Sena
    template_name = 'sena/sena_detail.html'


class CreateSena(CreateView):
    model = Sena
    form_class = SenaForm
    template_name = 'sena/sena_form.html'


class UpdateSena(UpdateView):
    model = Sena
    form_class = SenaForm
    template_name = 'sena/sena_form.html'


class DeleteSena(DeleteView):
    model = Sena
    success_url = '/'

