import django_filters
from django_filters import CharFilter
from .models import *

class SenaFilter(django_filters.FilterSet):
    titulo = CharFilter(label='Titulo',lookup_expr='istartswith')

    class Meta:
        model = Sena
        fields = ['titulo','categoria']
