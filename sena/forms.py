from django import forms
from .models import Sena


class SenaForm(forms.ModelForm):
    class Meta:
        model = Sena
        fields = ['titulo','video']
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'video': forms.FileInput(attrs={'class': 'form-control'}),
        }     
