$(function() {
    $(".alertas").hide();
    $(".siguiente").hide();
    $(".respuesta").click(function(){
        var respuesta = $(this).val();
        var titulo = $(".d-none").text();
        if (respuesta == titulo) {
            $(".alertas").addClass("alert-success");
            $(".alertas").removeClass("alert-danger");
            $(".texto").text("Opcion Correcta!! Bien hecho");
            $(".alertas").show();
            $(".siguiente").show();
        }
        else {
            $(".alertas").addClass("alert-danger");
            $(".alertas").removeClass("alert-success");
            $(".texto").text("Opcion Incorrecta!! Vuelva a intentarlo");
            $(".alertas").show();
        }
    });
});