from django.urls import path
from . import views
from .views import CreateSena, DetailSena, UpdateSena, DeleteSena

urlpatterns = [
    path('', views.about, name='sena-about'),
    path('catalog/', views.home, name='sena-home'),
    path('categories/', views.categories, name='category-list'),
    path('questionary/<int:id>', views.questionary, name='sena-quest'),
    path('new/', CreateSena.as_view(), name='sena-new'),
    path('<int:pk>/', DetailSena.as_view(), name='sena-detail'),
    path('<int:pk>/update/', UpdateSena.as_view(), name='sena-update'),
    path('<int:pk>/delete/', DeleteSena.as_view(), name='sena-delete'),
]
