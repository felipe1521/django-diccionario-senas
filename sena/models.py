from django.db import models
from django.utils import timezone
from django.urls import reverse

class Categoria(models.Model):
    titulo = models.CharField(max_length=50)

    def __str__(self):
        return self.titulo


class Sena(models.Model):
    titulo = models.CharField(max_length=50)
    fecha = models.DateField(default=timezone.now)
    video = models.FileField(upload_to='sena_pics')
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.titulo
    
    def get_absolute_url(self):
        return reverse('sena-detail',kwargs={'pk': self.pk})


class Cuestionario(models.Model):
    sena = models.ForeignKey(Sena, on_delete=models.CASCADE, null=True, blank=True)
    pregunta = models.CharField(max_length=200)
    respuesta1 = models.CharField(max_length=50)
    respuesta2 = models.CharField(max_length=50)
    respuesta3 = models.CharField(max_length=50)
    respuesta4 = models.CharField(max_length=50)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.pregunta
    