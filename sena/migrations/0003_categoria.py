# Generated by Django 3.0.8 on 2021-04-27 02:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sena', '0002_auto_20210402_1745'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=50)),
            ],
        ),
    ]
